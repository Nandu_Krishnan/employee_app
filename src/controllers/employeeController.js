const uuid = require('uuid').v4;

let employees = [
    {
        "name" : "Nandu Krishnan",
        "gender" : "male",
        "address" : "Add1",
        "phone" : "8282453568",
        "department" : "IT",
        "id" : "17dc8fe3-bae0-4b98-8036-3d13a76070bb"
    }
];

exports.getEmployees = (req, res) => {
    const employeeId = req.query.id
    const dept = req.query.department
    if(employeeId != null){
        let employee = employees.filter(employees => {
            return employees.id === employeeId
        })
        console.log("Employee Data : ")
        console.log(employee)
        res.json(employee)
    }
    else if(dept != null){
        let employee = employees.filter(employees => {
            return employees.department === dept
        })
        console.log("Department Emplyees Data : ")
        console.log(employee)
        res.json(employee)
    }
    else{
        res.json(employees);
    }
}

exports.createEmployee = (req, res) => {
    const employee = { ...req.body };
    employee.id = uuid();
    employees.push(employee);
    console.log("Creation Successful")
    res.json(employee);
  };
  
exports.updateEmployee = (req,res) => {
    const employeeId = req.params.id
	const employee = { ...req.body }
	let updateEmp = employees.filter( employees => {
		 if(employees.id === employeeId){
			 employees.phone = employee.phone
			 employees.address = employee.address
			 employees.gender = employee.gender
			 employees.department = employee.department
			 return true
		 } 
		 else
		 	return false
    });
    console.log("Updation Successful")
	console.log(updateEmp)
	res.json(updateEmp)
};

exports.deleteEmployee = (req,res) => {
	const employeeId = req.params.id
	let newEmployeelist = employees.filter(employees => {
		employees.id !== employeeId
    })
    employees=[...newEmployeelist]
    console.log("Deletion Successful")
	res.json(employees)
};
